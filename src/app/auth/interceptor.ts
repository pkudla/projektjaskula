import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authReq = req.clone({
            setHeaders: {'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImIzY2Y3N2E4MWIwODhjMTQxMTIxMWY5Yjg2ZTViNGY0ODhkODcwYTM4ZTkzNzAyNTBiMmI0N2I0MTlmYzMzYjQ3YzMxOWE1Y2ZmYjMwMWEwIn0.eyJhdWQiOiIyIiwianRpIjoiYjNjZjc3YTgxYjA4OGMxNDExMjExZjliODZlNWI0ZjQ4OGQ4NzBhMzhlOTM3MDI1MGIyYjQ3YjQxOWZjMzNiNDdjMzE5YTVjZmZiMzAxYTAiLCJpYXQiOjE1NDY1NTIyMTAsIm5iZiI6MTU0NjU1MjIxMCwiZXhwIjoxNTc4MDg4MjA5LCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.JOB6OcgAvkNPMP51hNENnto1YC7kuGG3qO0b1l0L_yltwCXTdtCTslkjxToTlvDREvr5NnOzlkU3D_JXu1AHJWFPekiDqVmA7k09tivTVVIto1Z1HuvYEHZLDGhXxUKtQPNJAz_0qBMWccBOmJA0YcO_Pl3WrXYfID3hj1qfpUKvTVpfmc--VnVMZ1UqoPhzgLaVwNw4j6pbz6IBrMYjt3Wa4iGhxYqyKy33g_TIVPIL8ujX5P0nD-90Q1iob5MEE9MCRcMiNdhTWeQDFN60ov__dPRTEhT_R6GHyNFdTCaXLwaWm3CQREpE9oGvg84wbFoupqeJMC0aHMDh6wTR0mFOoOJdXHJNaBDZZK7zAaAU3zm-zihEwnwrN_-lZIi40E2q3tD4wEfh6uZI3cqnrcaL9oeDdnh3WS8CpCu7vrTQawr1HCjDBgwM1Aihp_Oboy_TqxvdtD2hWRCm_mZim0_u58uIHJC0YkJzvwoWSUJxk84nl5jqXbsYac6KmTq4GHLEQifKIgy7agoTZxDz9zuHapApxrbr2MLOJHBoMUeMIMawdMNb4vz60lT1iGzYntyMR3ezeVIDFO6GC4_wkFu8vNFNEeOgQBnDuhaRiLxTmmLT9YeTWmBaBG4s2uQ86_4vXGkgrRE0JSvvRT4PQqnHwjitbquPdzuf7enmDfU'}
        });

        return next.handle(authReq);
    }

}
