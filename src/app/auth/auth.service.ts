import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly TOKEN_STORAGE_KEY = 'tokenID';

  private jwtHelperService = new JwtHelperService();

  constructor(private HTTP: HttpClient) {
  }


  saveToken(token: string) {
    localStorage.setItem(this.TOKEN_STORAGE_KEY, token);
  }

  isLoggedIn(): boolean {
    const token = localStorage.getItem(this.TOKEN_STORAGE_KEY);
    return token === undefined || token === 'undefined' ? false : !this.jwtHelperService.isTokenExpired(token);
  }

  getAuth(): Observable<any> {
    let headers = new HttpHeaders();

    headers = headers.set(
      'Content-Type', 'application/json',
    );
    headers = headers.set(
      'Accept', 'application/json',
    );
    return this.HTTP.post('http://145.239.83.211:81/api/getToken', {
      headers: headers,
      client_id: 2,
      client_secret: 'BKDywarucol6muQ3we5GwqQaFAjA4ZbofzlYifNX'
    });
  }
}
