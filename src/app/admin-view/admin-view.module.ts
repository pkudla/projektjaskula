import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminViewComponent} from './admin-view.component';
import {DashboardComponent} from './admin-content/panel-dashboard/dashboard.component';
import {AdminMenuComponent} from './admin-menu/admin-menu.component';
import {AuthGuard} from '../auth/auth.guard';
import {jqxChartComponent} from 'jqwidgets-scripts/jqwidgets-ng/jqxchart';
import {SectionService} from './admin-content/section/apartment-service/section.service';
import {CommonModule} from '@angular/common';
import { PanelArchiveComponent } from './admin-content/panel-archive/panel-archive.component';

const routes: Routes = [
  {
    path: '',
    component: AdminViewComponent, canActivate: [AuthGuard],
    children: [
      {
        path: 'section',
        loadChildren:
          './admin-content/section/section.module#SectionModule', canActivate: [AuthGuard]
      },
      {
        path: 'message',
        loadChildren:
          './admin-content/panel-message/message.module#MessageModule', canActivate: [AuthGuard]
      },
      {
        path: 'archive',
        loadChildren: './admin-content/panel-archive/archive.module#ArchiveModule', canActivate: [AuthGuard]
      },
      {
        path: '',
        component: DashboardComponent, canActivate: [AuthGuard]
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  declarations: [AdminViewComponent, DashboardComponent, AdminMenuComponent, jqxChartComponent],
  imports: [RouterModule.forChild(routes), CommonModule],
  providers: [SectionService]
})
export class AdminViewModule {
}
