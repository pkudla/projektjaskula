import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PanelArchiveComponent} from './panel-archive.component';

const route: Routes = [
  {
    path: '', component: PanelArchiveComponent
  }
]


@NgModule({
  declarations: [PanelArchiveComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class ArchiveModule { }
