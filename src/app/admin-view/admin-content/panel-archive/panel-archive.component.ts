import { Component, OnInit } from '@angular/core';
import {SectionService} from '../section/apartment-service/section.service';

@Component({
  selector: 'app-panel-archive',
  templateUrl: './panel-archive.component.html',
  styleUrls: ['./panel-archive.component.scss']
})
export class PanelArchiveComponent implements OnInit {

  public archive;
  public apartmentsList;

  constructor(private apartmentsService: SectionService) { }

  ngOnInit() {
    this.apartmentsService.findAll(this.archive = 1).subscribe(res => {
      this.apartmentsList = res.objects;
    });
  }

}
