import {Component, OnInit} from '@angular/core';
import {SectionService} from '../apartment-service/section.service';
import {Router} from '@angular/router';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-section-new',
  templateUrl: './section-new.component.html',
  styleUrls: ['./section-new.component.scss']
})
export class SectionNewComponent implements OnInit {

  constructor(private apartmentService: SectionService, private router: Router) {
  }

  private fileToUpload: File = null;
  private images = [];
  private selectedOption;


  options = [
    {name: 'Dostępne', value: 0},
    {name: 'Wynajęte', value: 1}
  ];



  inputFile(files: FileList) {
    this.fileToUpload = files.item(0);
    const formValues: FormData = new FormData();
    formValues.append('photo', this.fileToUpload, this.fileToUpload.name);
    this.apartmentService.sendImage(formValues).subscribe(res => this.images.push(res.photo_id));
  }



  ngOnInit() {
  }

  onFormSubmit(form) {
    const values = form.value;
    values.main_image_id = this.images[0];
    values.status = 0;
    values.images = this.images;
    values.status = this.selectedOption;
    if (values.price.length > 8) {
      values.price = 99999999;
    }

    if (!form.touched) {
      this.router.navigate(['admin/section']);
    } else {
      this.apartmentService.newApartment(values).subscribe();
      this.router.navigate(['admin/section']);
    }
  }
}
