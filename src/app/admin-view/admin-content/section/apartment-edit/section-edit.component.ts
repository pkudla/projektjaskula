import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SectionService} from '../apartment-service/section.service';

@Component({
  selector: 'app-section-edit',
  templateUrl: './section-edit.component.html',
  styleUrls: ['./section-edit.component.scss']
})
export class SectionEditComponent implements OnInit {

  apartmentID: string;

  apartment = {
    name: '',
    country: '',
    province: '',
    city: '',
    street: '',
    flat_number: '',
    floor_number: '',
    house_number: '',
    price: '',
    main_image_id: 0,
    short_description: '',
    description: '',
    area: '',
    images: [],
    status: '0'
  };

  private fileToUpload: File = null;
  private images = [];


  constructor(private service: SectionService, private route: ActivatedRoute, private router: Router) {
  }

  previewImage(photo_id) {
    this.images.push(photo_id);
    this.apartment.images.push(photo_id);
  }

  inputFile(files: FileList) {
    this.fileToUpload = files.item(0);
    const formValues: FormData = new FormData();
    formValues.append('photo', this.fileToUpload, this.fileToUpload.name);
    return this.service.sendImage(formValues).subscribe(res => this.previewImage(res.photo_id));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.apartmentID = params['id'];
      this.service.findByID(this.apartmentID).subscribe(res => {
        this.apartment = res.object;
        this.images = this.apartment.images;
      });
    });
  }

  onFormSubmit(form) {
    const values = form.value;
    if (values.price.length > 8) {
      values.price = 99999999;
    }
    values.main_image_id = this.images[0];
    values.images = this.images;

    this.service.edit(values, this.apartmentID).subscribe();
    this.router.navigate(['admin/section']);
  }
}
