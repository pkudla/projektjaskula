import {Component, OnInit} from '@angular/core';
import {SectionService} from '../apartment-service/section.service';

@Component({
  selector: 'app-section-list',
  templateUrl: './section-list.component.html',
  styleUrls: ['./section-list.component.scss']
})
export class SectionListComponent implements OnInit {

  public apartmentsList;
  public archive;

  constructor(private apartmentsService: SectionService) {
  }

  removeFlat(apartmentID) {
    return this.apartmentsService.removeFlat(apartmentID).subscribe(res => {
      console.log(res);
    });
  }


  ngOnInit() {
    this.apartmentsService.findAll(this.archive = 0).subscribe(res => {
      this.apartmentsList = res.objects;
    });
  }
}
