import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SectionListComponent} from './apartment-list/section-list.component';
import {SectionContentComponent} from '../panel-content/section-content.component';
import {Routes, RouterModule} from '@angular/router';
import {SectionEditComponent} from './apartment-edit/section-edit.component';
import {FormsModule} from '@angular/forms';
import {SectionNewComponent} from './apartment-new/section-new.component';
import {ApartmentModule} from './apartment-service/apartment.module';

const routes: Routes = [
  {
    path: '', component: SectionContentComponent, children: [
      {
        path: '', component: SectionListComponent
      },
      {
        path: 'new', component: SectionNewComponent
      },
      {
        path: ':id', component: SectionEditComponent
      }
    ]
  }

];

@NgModule({
  declarations: [
    SectionListComponent,
    SectionContentComponent,
    SectionEditComponent,
    SectionNewComponent,


  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ApartmentModule,


  ],
})
export class SectionModule {
}
