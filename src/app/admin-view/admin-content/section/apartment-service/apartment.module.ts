import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionService } from './section.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [SectionService]
})
export class ApartmentModule { }
