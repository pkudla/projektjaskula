import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class MessageService {

    private readonly API_URL = environment.apiUrl;

    constructor(private HTTP: HttpClient) {
    }

    getMessage(): Observable<any> {
        return this.HTTP.get(this.API_URL + '/api/forms/list');
    }
}
