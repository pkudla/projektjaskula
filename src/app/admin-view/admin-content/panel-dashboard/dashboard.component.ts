import {Component, OnInit} from '@angular/core';
import {SectionService} from '../section/apartment-service/section.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private service: SectionService) {
  }

  private tabObj = [];


  padding: any = {left: 20, top: 5, right: 20, bottom: 5};

  titlePadding: any = {left: 90, top: 0, right: 0, bottom: 10};


  xAxis =
    {
      dataField: 'name',
      gridLines: {visible: true},
      flip: false,
      displayText: 'Nazwa',
      labels: {
	angle: 90,
        rotationPoint: 'right',
	verticalAlignment: 'center',
	horizontalAlignment: 'right'
      }
    };

  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '90%';
    }

    return 850;
  }

  valueAxis: any =
    {
      flip: true,
      labels: {
        visible: true,
        formatFunction: (value: string) => {
          return parseInt(value);
        }
      }
    };

  seriesGroups: any[] =
    [
      {
        type: 'column',
        orientation: 'horizontal',
        columnsGapPercent: 70,
        toolTipFormatSettings: {thousandsSeparator: ','},
        series: [
          {dataField: 'visits', displayText: 'Wizyty'}
        ]
      }
    ];


  ngOnInit() {
    return this.service.getTopVisited().subscribe(res => this.tabObj = res.objects);
  }

}
