import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../auth/auth.service';
import {LoginService} from './login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm;

  private subscription;


  constructor(private formBuilder: FormBuilder, private Auth: AuthService, private Login: LoginService, private router: Router) {
  }


  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', Validators.required]
    });
  }

  onFormSubmit() {
    const user = this.loginForm.value;
    this.subscription = this.Login.login(user)
      .subscribe(res => {
        this.Auth.saveToken(res.user_token);
        this.router.navigate(['admin']);
      });
  }

}
