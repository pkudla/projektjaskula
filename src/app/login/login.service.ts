import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private HTTP: HttpClient) { }

  URL = environment.apiUrl;

  login(user): Observable<any> {
    return this.HTTP.post(`${this.URL}/api/auth/login`, user);

  }

}
