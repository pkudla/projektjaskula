import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {FooterService} from './footer/footer.service';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent, children: [
      {
        path: 'apartments',
        loadChildren: './apartments-list/apartments-list.module#ApartmentsListModule'
      },
      {
        path: '',
        loadChildren: './latest/latest.module#LatestModule'
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      },
    ],
  },
];

@NgModule({
  declarations: [HomePageComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ReactiveFormsModule
  ],
  providers: [FooterService]
})
export class HomePageModule {
}
