import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LatestComponent} from './latest.component';
import {Routes, RouterModule} from '@angular/router';
import {ApartmentModule} from 'src/app/admin-view/admin-content/section/apartment-service/apartment.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

const routes: Routes = [
  {
    path: '',
    component: LatestComponent
  }
];


@NgModule({
  declarations: [LatestComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ApartmentModule,
    MatProgressSpinnerModule
  ],
})
export class LatestModule {
}
