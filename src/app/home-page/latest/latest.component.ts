import { Component, OnInit } from '@angular/core';
import { SectionService } from 'src/app/admin-view/admin-content/section/apartment-service/section.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-latest',
  templateUrl: './latest.component.html',
  styleUrls: ['./latest.component.scss']
})
export class LatestComponent implements OnInit {

  public subscription;
  public apartments;
  public showSpinner: Boolean = true;

  constructor(private apartmentsList: SectionService) { }

  scrollFlats() {
    window.scrollTo(0, 500);
  }

  ngOnInit() {
    this.subscription = this.apartmentsList.newest().subscribe(res => {
      this.showSpinner = false;
      this.apartments = res.objects;
    });
  }

}
