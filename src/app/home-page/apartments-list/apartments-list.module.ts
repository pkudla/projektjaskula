import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {ApartmentsListComponent} from './apartments-list.component';
import {ApartmentModule} from 'src/app/admin-view/admin-content/section/apartment-service/apartment.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

const routes: Routes = [
  {
    path: '', component: ApartmentsListComponent, children: [
      {
        path: ':id',
        loadChildren: '../apartment-details/apartment-details.module#ApartmentDetailsModule'
      },
    ]
  },
];


@NgModule({
  declarations: [ApartmentsListComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    ApartmentModule,
    MatProgressSpinnerModule
  ],
  exports: [],
})
export class ApartmentsListModule {
}
