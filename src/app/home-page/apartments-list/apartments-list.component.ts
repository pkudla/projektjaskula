import {Component, OnInit} from '@angular/core';
import {SectionService} from 'src/app/admin-view/admin-content/section/apartment-service/section.service';

@Component({
  selector: 'app-apartments-list',
  templateUrl: './apartments-list.component.html',
  styleUrls: ['./apartments-list.component.scss']
})
export class ApartmentsListComponent implements OnInit {

  public apartments;
  public archive;

  showSpinner: boolean = true;


  constructor(private apartmentsService: SectionService) {
  }


  ngOnInit() {
    this.apartmentsService.findAll(this.archive = 0).subscribe(res => {
      this.showSpinner = false;
      this.apartments = res.objects;
    });
  }

}
