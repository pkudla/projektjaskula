import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FooterService } from './footer.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public form;
  public submitted = false;
  public subscription: Object = {};

  constructor(private formBuilder: FormBuilder, private service: FooterService, private AUTH: AuthService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'name': [null, Validators.required],
      'email': [null, [Validators.required, Validators.email]],
      'message': [null, Validators.required],
    });

    this.AUTH.getAuth().subscribe(res => {
      Object.assign(this.subscription, res);
    });
  }

  onFormSubmit() {
    if (this.form.status === 'VALID') {
      this.submitted = false;
      this.service.sendMessage(this.form.value, this.subscription).subscribe();
      return this.form.reset();
    } else {
      return this.submitted = true;

    }
  }

}
