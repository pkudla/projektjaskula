import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class FooterService {

  private readonly API_URL = environment.apiUrl;

  constructor(private HTTP: HttpClient) { }


  sendMessage(form, value): Observable<any> {
    let headers = new HttpHeaders();

    headers = headers.set(
      'Authorization', value.token_type + ' ' + value.access_token
    );

    return this.HTTP.post(this.API_URL + '/api/forms/create', form, { headers: headers }
    );
  }
}
