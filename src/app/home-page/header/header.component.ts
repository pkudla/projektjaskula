import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor() {
    }

    scrollHome() {
        window.scrollTo(0,0);
    }

    scrollFlats() {
        window.scrollTo(0, 500);
    }

    scrollContact() {
        window.scrollTo(0, 9000);
    }

    ngOnInit() {
    }
}


