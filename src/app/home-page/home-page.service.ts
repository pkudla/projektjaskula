import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable()
export class HomePageService {

  constructor(private HTTP: HttpClient,
              private sanitizer: DomSanitizer) {
  }

}
