import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ApartmentDetailsComponent} from './apartment-details.component';

const routes: Routes = [
  {
    path: '', component: ApartmentDetailsComponent
  }
];


@NgModule({
  declarations: [ApartmentDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ApartmentDetailsModule { }
