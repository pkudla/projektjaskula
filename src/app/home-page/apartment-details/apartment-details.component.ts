import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SectionService} from '../../admin-view/admin-content/section/apartment-service/section.service';

@Component({
  selector: 'app-apartment-details',
  templateUrl: './apartment-details.component.html',
  styleUrls: ['./apartment-details.component.scss']
})
export class ApartmentDetailsComponent implements OnInit {

  apartmentID;

  apartment = {
    name: '',
    country: '',
    province: '',
    city: '',
    street: '',
    flat_number: '',
    floor_number: '',
    price: '',
    main_image_id: 0,
    description: '',
    area: '',
    images: [],
  };

  constructor(private service: SectionService, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.apartmentID = params['id'];
      this.service.findByID(this.apartmentID).subscribe(res => {
        this.apartment = res.object;
      });
    });
  }

}
